import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';
//import { HomePage } from '../pages/home/home';
import { AccountPage } from '../pages/account/account';
import { AddKeyPage } from '../pages/addkey/addkey';
import { UploadKeyPage } from '../pages/upload/upload';
import { EncryptionPage } from '../pages/encrypt/encrypt';
import { DecryptionPage } from '../pages/decrypt/decrypt';
import { AboutPage } from '../pages/about/about';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { IonicStorageModule } from '@ionic/storage';
import { UserdataProvider } from '../providers/userdata/userdata';
import { KeyDataProvider } from '../providers/keydata/keydata';
import { IOSFilePicker } from '@ionic-native/file-picker';
import { File } from '@ionic-native/file';
import { FileChooser } from '@ionic-native/file-chooser';
import { FilePath } from '@ionic-native/file-path';
import { GooglePlus } from '@ionic-native/google-plus';

@NgModule({
  declarations: [
    MyApp,
    AccountPage,
    //HomePage,
    AddKeyPage,
    UploadKeyPage,
    EncryptionPage,
    DecryptionPage,
    AboutPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AccountPage,
    //HomePage,
    AddKeyPage,
    UploadKeyPage,
    EncryptionPage,
    DecryptionPage,
    AboutPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    UserdataProvider,
    KeyDataProvider,
    IOSFilePicker,
    File,
    FileChooser,
    GooglePlus,
    FilePath,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
  ]
})
export class AppModule {}

import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import { AccountPage } from '../pages/account/account';
import { AddKeyPage } from '../pages/addkey/addkey';
import { UploadKeyPage } from '../pages/upload/upload';
import { EncryptionPage } from '../pages/encrypt/encrypt';
import { DecryptionPage } from '../pages/decrypt/decrypt';
import { AboutPage } from '../pages/about/about';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = EncryptionPage;

  pages: Array<{title: string, component: any, icon: string }>;

  constructor(public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen) {
    this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [
      //{ title: 'Home', component: HomePage, icon: 'home' },
      { title: 'Account', component: AccountPage, icon: 'cloud-circle'}, 
      { title: 'Add Key', component: AddKeyPage, icon: 'add' },
      { title: 'Upload Key', component: UploadKeyPage, icon: 'cloud-upload' },
      { title: 'Encrypt', component: EncryptionPage, icon: 'lock' },
      { title: 'Decrypt', component: DecryptionPage, icon: 'unlock' },
      { title: 'About Us', component: AboutPage, icon: 'information'},
    ];

  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }
}

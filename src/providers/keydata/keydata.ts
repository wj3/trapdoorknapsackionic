//import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

/*
  Generated class for the UserdataProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class KeyDataProvider {

  keysize: any;
  privateKeyArr: any = [];
  publicKeyArr: any = [];
  modulus: number;
  multiplier: number;

  getKeysize() { return this.keysize; }
  getModulus() { return this.modulus; }
  getMultiplier() { return this.multiplier; }

  getPublicKeys() { return this.publicKeyArr; }
  getPrivateKeys() { return this.privateKeyArr; }

  setKeysize(size: any) {
      this.keysize = size;
  }

  setModulus(modulus: number) {
      this.modulus = modulus;
  }

  setMultiplier(multi: number) {
      this.multiplier = multi;
  }

  setPublickKeys(pk: any) {
    this.publicKeyArr = pk;
  }

  setPrivateKeys(sk: any) {
    this.privateKeyArr = sk;
  }

  reset() {
    this.keysize = 0;
  }

  constructor() {
    console.log('KeydataProvider Provider');
  }

}

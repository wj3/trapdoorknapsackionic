//import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

/*
  Generated class for the UserdataProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class UserdataProvider {

  email: string;
  userId: string;
  imageUrl: string;
  displayName: string;
  isLoggedIn: boolean;

  keysize: any;

  getLoginStatus() { return this.isLoggedIn; }
  getEmail() { return this.email; }
  getUserId() { return this.userId; }
  getImageUrl() { return this.imageUrl; }
  getDisplayName() { return this.displayName; }

  getKeysize() { return this.keysize; }

  setLoginStatus(status: boolean) { this.isLoggedIn = status; }

  setDetails(_email: string, _userId: string, _imageUrl, _displayName: string) {
    this.email = _email;
    this.userId = _userId;
    this.imageUrl = _imageUrl;
    this.displayName = _displayName;
  }

  setKeysize(size: any) {
      this.keysize = size;
  }

  reset() {
    this.displayName = "";
    this.email = "";  
    this.userId = "";
    this.imageUrl = "";

    this.isLoggedIn = false;
  }

  constructor() {
    console.log('UserdataProvider Provider');
  }

}

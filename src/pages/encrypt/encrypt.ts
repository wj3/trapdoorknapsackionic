import { Component } from '@angular/core';
import { NavController, NavParams, Platform, AlertController, Alert} from 'ionic-angular';
import { KeyDataProvider } from '../../providers/keydata/keydata';
import { IOSFilePicker } from '@ionic-native/file-picker';
import { File } from '@ionic-native/file';
import { FileChooser } from '@ionic-native/file-chooser';
import { FilePath } from '@ionic-native/file-path';
import { ViewChild } from '@angular/core';
import { Slides } from 'ionic-angular';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'page-encrypt',
  templateUrl: 'encrypt.html'
})
export class EncryptionPage {
    keysize = 1;
    privateKeyArr: any = [];
    publicKeyArr: any = [];
    modulus: number;
    multiplier: number;
    plaintext: string = "";
    encryptedText: string = "";
    showSlide: boolean = false;
    isEditing: boolean = true;
    explainedLinesArr: string[] = [];
    // maxDetailedExplain: number = 5;
    // maxLineExplain: number = 5;
    binPlainText:string = "";
    goToSlideNumber: number;
    totalSlide: number = 0;
    requiredCreateKey: boolean = false;

    @ViewChild(Slides) private slide: Slides;

  constructor(public navCtrl: NavController, public navParams: NavParams, private platform: Platform,  
    private keyData: KeyDataProvider, private iosFilePicker: IOSFilePicker, private file:File, 
    private fileChooser: FileChooser, private filePath: FilePath, private alertCtrl: AlertController, private storage: Storage) {
      var keysize: number;
      if(this.platform.is('core')) {
        //Core
        var sk = localStorage.getItem('privateKey');
        if(sk && sk!=''){
          this.privateKeyArr = sk.split(',')
          this.keysize = this.privateKeyArr.length
        }else{
          this.requiredCreateKey = true
          return
        }
        var pk = localStorage.getItem('publicKey');
        if(pk && pk!=''){
          this.publicKeyArr = pk.split(',')
          //this.keysize = this.publicKeyArr.length
        }else{
          this.requiredCreateKey = true
          return
        }
        var mod = localStorage.getItem('modulus');
        if(mod && mod!=''){
          this.modulus = parseInt(mod);
          if(isNaN(this.modulus)){
            this.modulus = 0
            this.requiredCreateKey = true
            return
          }
        }else{
          this.requiredCreateKey = true
          return
        }
        var multi = localStorage.getItem('multiplier');
        if(multi && multi!=''){
          this.multiplier = parseInt(multi);
          if(isNaN(this.multiplier)){
            this.multiplier = 0
            this.requiredCreateKey = true
            return
          }
        }else{
          this.requiredCreateKey = true
          return
        }
        this.keyData.privateKeyArr = this.privateKeyArr;
        this.keyData.publicKeyArr = this.publicKeyArr;
        this.keyData.modulus = this.modulus;
        this.keyData.multiplier = this.multiplier;
        this.keysize = this.privateKeyArr.length;
        this.requiredCreateKey = false;

      }else{
        //iOS or Android
        console.log('Mobile');
        storage.get('privateKey').then((private_key) => {
          console.log('Private Key from storage:::' + private_key);
          if(private_key && private_key!=''){
            private_key = String(private_key);
            this.privateKeyArr = private_key.split(',')
            this.keysize = this.privateKeyArr.length
          }else{
            this.requiredCreateKey = true
            return
          }
        })
        storage.get('publicKey').then((public_key) => {
          console.log('Public Key from storage:::' + public_key);
          if(public_key && public_key!=''){
            this.publicKeyArr = public_key.split(',')
            //this.keysize = this.publicKeyArr.length
          }else{
            this.requiredCreateKey = true
            return
          }
        })
        storage.get('modulus').then((modulus) => {
          console.log('Modulus from storage:::' + modulus);
          if(modulus && modulus!=''){
            this.modulus = parseInt(modulus);
            if(isNaN(this.modulus)){
              this.modulus = 0
              this.requiredCreateKey = true
              return
            }
          }else{
            this.requiredCreateKey = true
            return
          }
        })
        storage.get('multiplier').then((multiplier) => {
          console.log('Multiplier from storage:::' + multiplier);
          if(multiplier && multiplier!=''){
            this.multiplier = parseInt(multiplier);
            if(isNaN(this.multiplier)){
              this.multiplier = 0
              this.requiredCreateKey = true
              return
            }
          } else{
            this.requiredCreateKey = true
            return
          }
        })
        this.keyData.privateKeyArr = this.privateKeyArr;
        this.keyData.publicKeyArr = this.publicKeyArr;
        this.keyData.modulus = this.modulus;
        this.keyData.multiplier = this.multiplier;
        this.keysize = this.privateKeyArr.length;
        this.requiredCreateKey = false
      }
}

  plaintextChanged(event) {
    this.plaintext = event.value;
  }

  encrypt() {
    if (this.requiredCreateKey){
      //console.log('Encrypt::: KeyDataProvider: ' + this.keyData.getKeysize());
      //this.requiredCreateKey = true;
      this.keysize = 8;
      this.modulus = 256;
      this.multiplier = 7;
      this.publicKeyArr = [7,14,28,56,112,224,192,128];
      this.privateKeyArr = [1,2,4,8,16,32,64,128];
    }
    console.log('Encrypt:::Key size: ' + this.keysize);
    console.log('Encrypt:::Modulus: ' + this.modulus);
    console.log('Encrypt:::Multiplier: ' + this.multiplier);
    console.log('Encrypt:::Public Keys: ' + this.publicKeyArr);
    // convert to 8-bit binary
    this.binPlainText = this.textToBin(this.plaintext);
    console.log("plaintext in binary (before padding): " + this.binPlainText);

    // padding (size of knapsack algorithm)
    while ((this.binPlainText.length % this.keysize) != 0){
      this.binPlainText = "0" + this.binPlainText;
    }
    console.log("plaintext in binary (after padding): " + this.binPlainText);

    // encryption process
    this.encryptedText = "";
    var temp = 0;
    // var binaryExplain = this.binPlainText.length <= 128 ? this.binPlainText : this.binPlainText.substring(0,128);
    // this.encryptionExplained = "First convert the each char of the plaintext ascii values into binary:\n" + binaryExplain + "\n";
    // this.encryptionExplained+="and compute the load T = Sum(xi * bi) using the public key.\n";
    // var maxBinaryChar = 64;
    // var explainedLines:string = "";
    this.explainedLinesArr.push("Firstly, convert each character in the plaintext ASCII values to binary\n" + this.binPlainText + "\n");
    var calculationExplain = '';
    for(var k=0; k<this.binPlainText.length; k++){
        var value = parseInt(this.binPlainText.charAt(k)); //binary value
        var multi = this.publicKeyArr[k%this.keysize]; //publick key
        if(k%this.keysize == 0 && k!=0){
            this.encryptedText+=temp + ',';
            temp = value*multi;
            //Explain the calculation process
            // if(k<this.maxDetailedExplain*this.keysize){
              this.explainedLinesArr.push(this.getDetailedExplain(k));
            // }
            // if(k<maxBinaryChar){
            // if(k<this.maxLineExplain*this.keysize){
              calculationExplain+='T' + (k/this.keysize) + ' = ('+ temp;
            // }
        }else{
            temp += value*multi;
            //Explain the calculation process
            if(k==0){
              // calculationExpalin='T1 = (' + temp;
              this.explainedLinesArr.push("Then, split the binary into multiple blocks of keysize " + this.keysize + " and apply the formula T = SUM( b[i] * x[i] ), where b is the binary and x is the public key.\n\n" + this.getDetailedExplain(k));
              calculationExplain='T' + (k/this.keysize) + ' = (' + temp;
            // }else if(k<maxBinaryChar){
            // }else if(k<this.maxLineExplain*this.keysize){
            }else{
              calculationExplain+=' + ' + value*multi;
              if(k%this.keysize == this.keysize-1) {
                calculationExplain+=') = ' + temp + '\n'; 
              }
            }
        }
        if(k==this.binPlainText.length-1){
            this.encryptedText+=temp + ', ';
        }
    }
    // if(this.binPlainText.length>this.maxLineExplain*this.keysize){
    //   calculationExplain+='... ... ... ...\n'
    // }
    // this.encryptionExplained+= calculationExpalin;
    // this.encryptionExplained+='The encrypted message is: (' + this.encryptedText + ')';
    // this.encryptionLines = this.encryptionExplained.split('\n');
    this.explainedLinesArr.push(calculationExplain);
    this.explainedLinesArr.push('The encrypted message is ' + this.encryptedText);
    console.log("Encrypted text: " + this.encryptedText);
    
    this.buildSlide();
  }

  upload() {
    if(this.platform.is('ios')) {
      this.iosFilePicker.pickFile().then(uri => 
        {console.log(uri)
        var correctPath = uri.substr(0, uri.lastIndexOf('/') + 1);
        var currentName = uri.substring(uri.lastIndexOf('/') + 1);
        var nameParts = currentName.split('.');
        if(nameParts.length>1) {
          var postfix = nameParts[nameParts.length-1];
          console.log('extension:::' + postfix);
          if(postfix!='txt'){
            //Show error
            this.showError('File Error', 'Only txt file are supported.', 'OK')
            return;
          }
        }else{
          this.showError('File Error', 'Invalid file.', 'OK')
          return;
        }
        this.file.readAsText("file:///" + correctPath, currentName).then(result=>{                           
          console.log('File content:::' + result)
          this.plaintext = result
        })}
      ).catch(err => 
          console.log('Error', err));
    }else if(this.platform.is('android')) {
      this.fileChooser.open().then(uri => 
        {console.log(uri)
        this.filePath.resolveNativePath(uri)
          .then(resolvedUri => 
            {
              console.log(resolvedUri)
              var correctPath = resolvedUri.substr(0, resolvedUri.lastIndexOf('/') + 1);
              var currentName = resolvedUri.substring(resolvedUri.lastIndexOf('/') + 1);
              //console.log('filename:::' + currentName);
              var nameParts = currentName.split('.');
              if(nameParts.length>1) {
                var postfix = nameParts[nameParts.length-1];
                console.log('extension:::' + postfix);
                if(postfix!='txt'){
                  //Show error
                  this.showError('File Error', 'Only txt file are supported.', 'OK')
                  return;
                }
              }else{
                this.showError('File Error', 'Invalid file.', 'OK')
                return;
              }
              this.file.readAsText(correctPath, currentName).then(result=>{                           
              console.log('File content:::' + result)
              this.plaintext = result
        })
            }).catch(err => console.log(err));
        }
      ).catch(e => console.log(e));
    }
  }

  textToBin(text:string){
    var binary:string = "";
    var binText:string = "";
    for (var i = 0; i < text.length; i++){
      binary = text[i].charCodeAt(0).toString(2);
      // padding (8-bit)
      while (binary.length < 8){
      binary = "0" + binary;
      }
      binText += binary;
    }
    return binText;
  }

  buildSlide() {
    this.showSlide = true;
    this.isEditing = false;
  }
  
  updateSlide() {
    this.slide.autoHeight = true;
    this.slide.resize();
    if (this.slide.getActiveIndex() == this.slide.length()-1){
      this.slide.stopAutoplay();
    }
  }

  getTotalSlide(){
    this.totalSlide = this.slide.length();
    console.log('Number of slides: ' + this.slide.length());
  }

  updateCurrentSlide(event) {
    this.goToSlideNumber = event.value;
    console.log("Entered slide: " + this.goToSlideNumber);
  }

  goToSlide(){
    if ((this.goToSlideNumber >= 1) && (this.goToSlideNumber <= this.totalSlide)){
      this.slide.slideTo(this.goToSlideNumber-1, 0);
      console.log("Go to slide: " + this.goToSlideNumber);
    }
  }

  getDetailedExplain(startIndex:number){
    // console.log("Block " + (startIndex/this.keysize));
    var explainedLines = ("Block " + (startIndex/this.keysize) +" = " + this.binPlainText.substring(startIndex, startIndex+this.keysize) + "\n\n");
    var sumExplain:string = "Sum of T[" + (startIndex/this.keysize) + "] = ";
    var sumOfT = 0;
    for (var i=startIndex; i<(startIndex+this.keysize); i++){
        sumOfT += (parseInt(this.binPlainText[i]) * this.publicKeyArr[i%this.keysize]);
        if (i==(startIndex+this.keysize-1)){
            sumExplain += (parseInt(this.binPlainText[i]) * this.publicKeyArr[i%this.keysize] + " = " + sumOfT);
        }
        else {
            sumExplain += (parseInt(this.binPlainText[i]) * this.publicKeyArr[i%this.keysize] + " + ");
        }
        explainedLines += ("b[" + (i%this.keysize) + "] = " + this.binPlainText[i] + "\n");
        explainedLines += ("x[" + (i%this.keysize) + "] = " + this.publicKeyArr[i%this.keysize] + "\n");
        explainedLines += ("T = " + this.binPlainText[i] + " * " + this.publicKeyArr[i%this.keysize] + " = " + (parseInt(this.binPlainText[i]) * this.publicKeyArr[i%this.keysize]) + "\n\n");
    }
    // console.log("startIndex : " + startIndex);
    // console.log("this.keysize : " + this.keysize);
    // console.log("(startIndex+this.keysize) : " + (startIndex+this.keysize));
    return (explainedLines + sumExplain);
  }

  showError(title, subTitle, buttonTitle) {
    let alert = this.alertCtrl.create({
        title: title,
        subTitle: subTitle,
        buttons: [buttonTitle]
      });
      alert.present();
  }
}

import { Component } from '@angular/core';
import { NavController, NavParams, Platform, AlertController } from 'ionic-angular';
import { IOSFilePicker } from '@ionic-native/file-picker';
import { File } from '@ionic-native/file';
import { FileChooser } from '@ionic-native/file-chooser';
import { FilePath } from '@ionic-native/file-path';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'page-uploadkey',
  templateUrl: 'upload.html'
})
export class UploadKeyPage {
  privateKey: string = ''
  privateKeyArr: any = [];
  publicKeyArr: any = [];
  privateKeyStr: string = '';
  publicKeyStr: string = '';
  modulus: number;
  multiplier: number;
  fileLoaded: boolean = false;

  constructor(public navCtrl: NavController, public navParams: NavParams, private platform: Platform, private iosFilePicker: IOSFilePicker, private file:File, 
    private fileChooser: FileChooser, private filePath: FilePath, private alertCtrl: AlertController, private storage: Storage) {

  }

  uploadKey() {
    if(this.platform.is('ios')) {
      this.iosFilePicker.pickFile().then(uri => 
        {console.log(uri)
        var correctPath = uri.substr(0, uri.lastIndexOf('/') + 1);
        var currentName = uri.substring(uri.lastIndexOf('/') + 1);
        var nameParts = currentName.split('.');
        if(nameParts.length>1) {
          var postfix = nameParts[nameParts.length-1];
          console.log('extension:::' + postfix);
          if(postfix!='txt'){
            //Show error
            this.showPopup('File Error', 'Only txt file are supported.', 'OK')
            return;
          }
        }else{
          this.showPopup('File Error', 'Invalid file.', 'OK')
          return;
        }
        this.file.readAsText("file:///" + correctPath, currentName).then(result=>{                           
          console.log('File content:::' + result)
          this.processKey(result);
          this.fileLoaded = true;
        })}
      ).catch(err => 
          console.log('Error', err));
    }else if(this.platform.is('android')) {
      this.fileChooser.open().then(uri => 
        {console.log(uri)
        this.filePath.resolveNativePath(uri)
          .then(resolvedUri => 
            {
              console.log(resolvedUri)
              var correctPath = resolvedUri.substr(0, resolvedUri.lastIndexOf('/') + 1);
              var currentName = resolvedUri.substring(resolvedUri.lastIndexOf('/') + 1);
              var nameParts = currentName.split('.');
              if(nameParts.length>1) {
                var postfix = nameParts[nameParts.length-1];
                console.log('extension:::' + postfix);
                if(postfix!='txt'){
                //Show error
                this.showPopup('File Error', 'Only txt file are supported.', 'OK')
                return;
                }
              }else{
                this.showPopup('File Error', 'Invalid file.', 'OK')
                return;
              }
              this.file.readAsText(correctPath, currentName).then(result=>{                           
              console.log('File content:::' + result)
              this.processKey(result);
              this.fileLoaded = true;
        })
            }).catch(err => console.log(err));
        }
      ).catch(e => console.log(e));
    }

  }

  saveKey() {
    if(this.platform.is('core')) {
      localStorage.setItem('privateKey', this.privateKeyStr);
      localStorage.setItem('publicKey', this.publicKeyStr);
      localStorage.setItem('modulus', String(this.modulus));
      localStorage.setItem('multiplier', String(this.multiplier));
      this.showPopup('Success', 'Keys saved!', 'Nice');
    }else{
      console.log('Private Key:::' + this.privateKeyStr);
      console.log('Public Key:::' + this.publicKeyStr);
      this.storage.set('privateKey', this.privateKeyStr);
      this.storage.set('publicKey', this.publicKeyStr);
      this.storage.set('modulus', this.modulus);
      this.storage.set('multiplier', this.multiplier);
      this.showPopup('Success', 'Keys saved!', 'Nice');
    }
  }

  cancel() {
    //reset
    this.privateKey = ''
    this.privateKeyStr = ''
    this.modulus = 0
    this.multiplier = 0
    this.fileLoaded = false;
  }

  generateKey() {
    if((this.modulus>0) && (this.multiplier>0) && (this.validateKeys())){
        this.publicKeyStr = '';
        for(var i=0;i<this.privateKeyArr.length;i++) {
            var keyVal = parseInt(this.privateKeyArr[i]);
            var value = this.privateKeyArr[i] * this.multiplier % this.modulus;
            this.publicKeyArr.push(value);
            this.publicKeyStr+=value;
            if(i<this.privateKeyArr.length-1){
              this.publicKeyStr+=',';
            }
        }
    }else{
      console.log('Modulus or Multiplier')
    }
  }

  validateKeys() {
    var total = 0;
    for(var i=0;i<this.privateKeyArr.length;i++) {
      total+=parseInt(this.privateKeyArr[i]);
    }
    if(this.modulus<=total) {
        this.showPopup('Invalid Modulus', 'Modulus must be greater than the sum of private keys', 'Got it');
        return false;
    }
    //check co-prime (multiplier relatively coprime to modulus)
    if(this.isCoPrime(this.multiplier, this.modulus)){
      //this.multiplierValid = true;
    }else{
      this.showPopup('Invalid Multiplier', 'Multiplier must be relatively co-prime to Modulus', 'Got it');
      return false;
  }
    return true;
  }

  isSuperIncreasing(keyArr: any[]) {
    var preSum = 0;
    for(var i=0; i<keyArr.length; i++) {
        var num = keyArr[i];
        console.log('Current: ' + num + ', previous Sum:' + preSum);
        if( num <= preSum) {
            return false;
        }
        preSum+=num;
    }
    return true;
  }

  isCoPrime(n:number, m:number) {
    if (this.xgcd(n,m)[0] == 1){
      return true;
    }else{
      return false;
    }
  }

  showPopup(title, subTitle, buttonTitle) {
    let alert = this.alertCtrl.create({
        title: title,
        subTitle: subTitle,
        buttons: [buttonTitle]
      });
      alert.present();
  }

  processKey(keyStr) {
    var keyParts = keyStr.split('|');
    if(keyParts.length != 3) {
      this.showPopup('Invalid format', 'Please refer to the correct format', 'Got it');
      return;
    } 
    var secretKey = keyParts[0];
    var modulus = parseInt(keyParts[1]);
    var multi = parseInt(keyParts[2]);
    this.privateKeyStr = secretKey;
    this.privateKeyArr = secretKey.split(',')
    this.modulus = modulus;
    this.multiplier = multi;
    this.privateKey = 'Your private key is: ' + secretKey + '. \n Modulus is: ' + this.modulus + ', ' + 'Multiplier is: ' + multi;  
    this.generateKey();
  }

  xgcd(a, b){
    a = +a;
    b = +b;
    
    if (a !== a || b !== b){
        return [NaN, NaN, NaN];
    }
    
    if (a === Infinity || a === -Infinity || b === Infinity || b === -Infinity){
        return [Infinity, Infinity, Infinity];
    }
    
    // Checks if a or b are decimals
    if ((a % 1 !== 0) || (b % 1 !== 0)){
        return false;
    }
    
    var signX = (a < 0) ? -1 : 1,
    signY = (b < 0) ? -1 : 1,
    x = 0,
    y = 1,
    u = 1,
    v = 0,
    q, r, m, n;
    a = Math.abs(a);
    b = Math.abs(b);
    
    while (a !== 0){
        q = Math.floor(b / a);
        r = b % a;
        m = x - u * q;
        n = y - v * q;
        b = a;
        a = r;
        x = u;
        y = v;
        u = m;
        v = n;
    }
    return [b, signX * x, signY * y];
  }

}

import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Platform } from 'ionic-angular';
import { GooglePlus } from '@ionic-native/google-plus';
import { Storage } from '@ionic/storage';
import { UserdataProvider } from '../../providers/userdata/userdata';

@Component({
  selector: 'page-account',
  templateUrl: 'account.html',
  providers: [GooglePlus]
})
export class AccountPage {

  email: string;
  userId: string;
  imageUrl: string;
  displayName: string;
  isLoggedIn: boolean;

  private androidClientId: string = '682172767304-ocpf98ssh87k0c7kc7n5f9ivhiosvqjs.apps.googleusercontent.com';
  private webClientId: string = '682172767304-eg7afnn1ipjk5anqd7rca94v15t9v8kg.apps.googleusercontent.com';

  constructor(public navCtrl: NavController, public navParams: NavParams, private platform: Platform, private googlePlus: GooglePlus, private storage: Storage, private userdata: UserdataProvider) {
      platform.ready().then(() => {
          console.log("Platform: " + platform.platforms());
          if(platform.is('core')) {
            status = localStorage.getItem('isLoggedIn');
            console.log("Login:" + status);
            if(status=='true'){
              this.userdata.isLoggedIn = true;
              this.userdata.email = localStorage.getItem('email');
              this.userdata.displayName = localStorage.getItem('displayName');
              this.userdata.userId = localStorage.getItem("userId");
              this.userdata.imageUrl = localStorage.getItem('imageUrl');
            }else{
              this.userdata.isLoggedIn = false;
              this.userdata.email = '';
              this.userdata.displayName = '';
              this.userdata.userId = '';
              this.userdata.imageUrl = '';
            }
          }else if(platform.is('ios') || platform.is('android')) {
            storage.get('isLoggedIn').then((val) => {
              status = val;
              if(status=='true'){
                this.userdata.isLoggedIn = true;
                storage.get('email').then((val) => {
                  this.userdata.email = val;
                });
                storage.get('displayName').then((val) => {
                  this.userdata.displayName = val;
                });
                storage.get('userId').then((val) => {
                  this.userdata.userId = val;
                });
                storage.get('imageUrl').then((val) => {
                  this.userdata.imageUrl = val;
                });
              }else{
                this.userdata.isLoggedIn = false;
                this.userdata.email = '';
                this.userdata.displayName = '';
                this.userdata.userId = '';
                this.userdata.imageUrl = '';
              }
            })
          }
        });
  }

  login() {
    var clientId: string = '';
    if(this.platform.is('android')) {
        clientId = this.androidClientId;
    }else if(this.platform.is('ios')){
        clientId = '';
    }else{
        clientId = this.webClientId;
    }
    this.googlePlus.login({
      'webClientId': clientId
    }).then((res) => {
      console.log(res);
      this.userdata.displayName = res.displayName;
      this.userdata.email = res.email;
      this.userdata.userId = res.userId;
      this.userdata.imageUrl = res.imageUrl;
      this.userdata.isLoggedIn = true;

      if(this.platform.is('core')) {
        localStorage.setItem('isLoggedIn', 'true');
        localStorage.setItem('email', this.userdata.email);
        localStorage.setItem('displayName', this.userdata.displayName);
        localStorage.setItem('userId', this.userdata.userId);
        localStorage.setItem('imageUrl', this.userdata.imageUrl);
      }else{
        this.storage.set('isLoggedIn', 'true');
        this.storage.set('email', this.userdata.email);
        this.storage.set('displayName', this.userdata.displayName);
        this.storage.set('userId', this.userdata.userId);
        this.storage.set('imageUrl', this.userdata.imageUrl);
      }

    }, (err) => {
      console.log('Sign In error:::' + err);
    });
  }

  logout(){
    this.googlePlus.logout().then(res => {
      console.log(res);
      this.isLoggedIn = false;
      if(this.platform.is('core')) {
        localStorage.setItem('isLoggedIn', 'false');
        localStorage.setItem('email', '');
        localStorage.setItem('displayName', '');
        localStorage.setItem('userId', '');
        localStorage.setItem('imageUrl', '');
      }else{
        this.storage.set('isLoggedIn', 'false');
        this.storage.set('email', '');
        this.storage.set('displayName', '');
        this.storage.set('userId', '');
        this.storage.set('imageUrl', '');
      }
      this.userdata.displayName = '';
      this.userdata.email = '';
      this.userdata.userId = '';
      this.userdata.imageUrl = '';
      this.userdata.isLoggedIn = false;
      //this.nativeStorage.setItem('loginInfo', {isLoggedIn: false, email: '', displayName: '', userId: '', imageUrl: ''})
    }).catch(err => console.error(err));
  }
}

import { Component } from '@angular/core';
import { NavController, NavParams, Platform, AlertController } from 'ionic-angular';
import { KeyDataProvider } from '../../providers/keydata/keydata';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'page-addkey',
  templateUrl: 'addkey.html'
})
export class AddKeyPage {
  keysize = 0;
  items: any = [];
  privateKeyArr: any = [];
  publicKeyArr: any = [];
  privateKeyStr: string = '';
  publicKeyStr: string = '';
  sum = 0;
  isEditing: boolean = true;
  hasSize: boolean = false;
  keysEntered: boolean = false;
  keyValid: boolean = false;
  showNext: boolean = true;
  pkGenerated: boolean = false;
  modulus: number;
  multiplier: number;
  modulusValid: boolean = false;
  multiplierValid: boolean = false;
  // pkExplained: string = '';
  pkExplainedLines: any = [];
  showSuperincreasingExplain = false;
  showCoprimeExplain = false;

  constructor(public navCtrl: NavController, public navParams: NavParams, private platform: Platform, 
    private alertCtrl: AlertController, private keyData: KeyDataProvider, private storage: Storage) {
    
  }

  sizeChange(val: any) {
    console.log('AddKey:::Key size: ', val);
    this.keysize = val;
    this.hasSize = true;
    this.items = [];
    for(var i = 0; i < this.keysize; i++) {
      this.items.push({'id': i+1, 'value': 0});
    }
  }

  numberChagned(event, index) {
    var flag: boolean = true;
    console.log("Index (" + index + ") You entered: " + event.value);
    for(let item of this.items) {
      if(item.id == index+1) {
        console.log('Item: ' + (index+1));
        item.value = event.value == "" ? 0 : event.value;
      }
      if(item.value == 0 || item.value == '') {
        flag = false;
      }
    }
    this.keysEntered = flag;
    this.sum = 0;
    for(let item of this.items) {
      //console.log("id: " + item.id + ", value:" + item.value);
      this.sum+= parseInt(item.value);
    }
    console.log("Sum is " + this.sum);
  }

  multiplierChanged(event) {
    if (event==''){
      return;
    }else{
      console.log("Mutiplier entered: " + event.value);
      this.multiplier = parseInt(event.value);
    }
  }

  modulusChanged(event) {
    /*if(event.value.length > 7) {
        event.value = event.value.substr(0, 7);
    }*/
    if (event==''){
      return;
    }else{
      console.log("Modulus entered: " + event.value);
      this.modulus = parseInt(event.value);
    }
  }

  goNext() {
    var keyArr: any = [];
    this.privateKeyArr = [];
    this.privateKeyStr = '';
    for(let item of this.items) {
        var num = parseInt(item.value);
        keyArr.push(num);
        this.privateKeyStr+=num + ',';
    }
    this.privateKeyStr = this.privateKeyStr.substring(0, this.privateKeyStr.lastIndexOf(','));
    //this.privateKeyStr.substr(0,this.privateKeyStr.length-2);
    // keyArr.sort(function(a, b) {
    //     return parseInt(a) - parseInt(b);
    // });
    console.log('Private keys entered:' + keyArr);
    if(this.isSuperIncreasing(keyArr)){
        this.privateKeyArr = keyArr;
        this.keyValid = true;
        this.showNext = false;
        this.isEditing = false;
    }else{
        this.keyValid = false;
        this.isEditing = true;
        this.showPopup('Invalid Key', 'The keys are not super-increasing.', 'Got it');
        this.showSuperincreasingExplain = true;
        // this.showError('What are Super-increasing Keys?', 'A superincreasing sequence shows that the next term of the sequence is greater than the sum of all preceding terms.\n\nExplanation:\n1+2+3+9 = 15\n10 is less than 15.', 'Got it')
    }
  }

  generateKey() {
    if((this.modulus>0) && (this.multiplier>0) && (this.validateKeys())){
        this.keyData.setKeysize(this.keysize);
        this.keyData.setPrivateKeys(this.privateKeyArr);

        this.keyData.setModulus(this.modulus);
        this.keyData.setMultiplier(this.multiplier);
        // this.pkExplained = 'To calculate public keys, apply the formula\npublicKey[i] = (privateKey[i] * multiplier) % modulus\n\n';
        // this.pkExplainedLines.push('The public keys are not presented in a super-increasing sequence. It is calculated using private keys, modulus, and multiplier. This process is known as the transformation of the super-increasing knapsack algorithm to the non-superincreasing knapsack algorithm.\n');
        // this.pkExplainedLines.push('To calculate public keys, apply the formula\n');
        // this.pkExplainedLines.push('publicKey[i] = (privateKey[i] * multiplier) mod modulus\n\n');
        this.publicKeyStr = '';
        for(var i=0;i<this.privateKeyArr.length;i++) {
            var value = this.privateKeyArr[i] * this.multiplier % this.modulus;
            this.publicKeyArr.push(value);
            this.publicKeyStr+=value;
            if(i<this.privateKeyArr.length-1){
              this.publicKeyStr+=',';
            }
            // this.pkExplained+='Public Key[' + (i+1) + ']: ' + this.privateKeyArr[i] + ' * ' + this.multiplier + ' % ' + this.modulus +  ' = ' +  value + '\n';
            this.pkExplainedLines.push('PublicKey[' + (i+1) + '] = (' + this.privateKeyArr[i] + ' * ' + this.multiplier + ') mod ' + this.modulus +  ' = ' +  value + '\n');
        }
        // this.pkExplained+='So the public key is: ' + this.publicKeyStr;
        // this.pkExplainedLines = this.pkExplained.split('\n');
        this.pkExplainedLines.push('So the public key is ' + this.publicKeyStr);
        this.pkGenerated = true;
        this.keyData.setPublickKeys(this.publicKeyArr);
    }
  }

  saveKey() {
    if(this.platform.is('core')) {
      localStorage.setItem('privateKey', this.privateKeyStr);
      localStorage.setItem('publicKey', this.publicKeyArr);
      localStorage.setItem('modulus', String(this.modulus));
      localStorage.setItem('multiplier', String(this.multiplier));
      this.showPopup('Success', 'Keys saved!', 'Nice');
    }else{
      //console.log('Private Key:::' + this.privateKeyStr);
      this.storage.set('privateKey', this.privateKeyStr);
      this.storage.set('publicKey', this.publicKeyStr);
      this.storage.set('modulus', this.modulus);
      this.storage.set('multiplier', this.multiplier);
      console.log('Addkey:::Private Key:' + this.privateKeyStr);
      console.log('Addkey:::Public Key:' + this.publicKeyStr);
      console.log('Addkey:::Modulus:' + this.modulus);
      console.log('Addkey:::Multiplier:' + this.multiplier);
      this.showPopup('Success', 'Keys saved!', 'Nice');
    }
  }

  cancel() {
    this.isEditing = true;
    this.hasSize = false;
    this.keysEntered = false;
    this.keyValid = false;
    this.showNext = true;
    this.pkGenerated = false;
    this.modulus = 0;
    this.multiplier = 0;
    this.modulusValid = false;
    this.multiplierValid = false;
  }

  validateKeys() {
    if(this.modulus<=this.sum) {
        this.modulusValid = false;
        this.showPopup('Invalid Modulus', 'Modulus must be greater than the sum of private keys', 'Got it');
        return false;
    }else{
        this.modulusValid = true;
    }
    //check co-prime (multiplier relatively coprime to modulus)
    if(this.isCoPrime(this.multiplier, this.modulus)){
      this.multiplierValid = true;
    }else{
      this.multiplierValid = false;
      this.showCoprimeExplain = true;
      this.showPopup('Invalid Multiplier', 'Multiplier must be relatively co-prime to Modulus', 'Got it');
      // this.showError('Hint!', 'You can use GCD to check if both numbers are relatively prime to each other. If GCD(x,y)=1, it shows that x is relatively prime to y.', 'Got it')
      return false;
  }
    return true;
  }

  isSuperIncreasing(keyArr: any[]) {
    var preSum = 0;
    for(var i=0; i<keyArr.length; i++) {
        var num = keyArr[i];
        console.log('Current: ' + num + ', previous Sum:' + preSum);
        if( num <= preSum) {
            return false;
        }
        preSum+=num;
    }
    return true;
  }

  isCoPrime(n:number, m:number) {
    if (this.xgcd(n,m)[0] == 1){
      return true;
    }else{
      return false;
    }
  }

  showPopup(title, subTitle, buttonTitle) {
    let alert = this.alertCtrl.create({
        title: title,
        subTitle: subTitle,
        buttons: [buttonTitle]
      });
      alert.present();
  }

  xgcd(a, b){
    a = +a;
    b = +b;
    
    if (a !== a || b !== b){
        return [NaN, NaN, NaN];
    }
    
    if (a === Infinity || a === -Infinity || b === Infinity || b === -Infinity){
        return [Infinity, Infinity, Infinity];
    }
    
    // Checks if a or b are decimals
    if ((a % 1 !== 0) || (b % 1 !== 0)){
        return false;
    }
    
    var signX = (a < 0) ? -1 : 1,
    signY = (b < 0) ? -1 : 1,
    x = 0,
    y = 1,
    u = 1,
    v = 0,
    q, r, m, n;
    a = Math.abs(a);
    b = Math.abs(b);
    
    while (a !== 0){
        q = Math.floor(b / a);
        r = b % a;
        m = x - u * q;
        n = y - v * q;
        b = a;
        a = r;
        x = u;
        y = v;
        u = m;
        v = n;
    }
    return [b, signX * x, signY * y];
  }

}

import { Component } from '@angular/core';
import { NavController, NavParams, Platform, AlertController } from 'ionic-angular';
import { KeyDataProvider } from '../../providers/keydata/keydata';
import { IOSFilePicker } from '@ionic-native/file-picker';
import { File } from '@ionic-native/file';
import { FileChooser } from '@ionic-native/file-chooser';
import { FilePath } from '@ionic-native/file-path';
import { ViewChild } from '@angular/core';
import { Slides } from 'ionic-angular';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'page-decrypt',
  templateUrl: 'decrypt.html'
})
export class DecryptionPage {
    keysize = 0;
    privateKeyArr: any = [];
    publicKeyArr: any = [];
    modulus: number;
    multiplier: number;
    ciphertext:string = "";
    decryptedText:string = "";
    showSlide: boolean = false;
    isEditing: boolean = true;
    explainedLinesArr: string[] = [];
    binText:string = "";
    goToSlideNumber: number;
    totalSlide: number = 0;
    requiredCreateKey: boolean = false;

    @ViewChild(Slides) private slide: Slides;

  constructor(public navCtrl: NavController, public navParams: NavParams, private platform: Platform, 
    private keyData: KeyDataProvider, private iosFilePicker: IOSFilePicker, private file:File, 
    private fileChooser: FileChooser, private filePath: FilePath, private alertCtrl: AlertController, private storage: Storage) {
        var keysize: number;
        if(this.platform.is('core')) {
            //Core
            var sk = localStorage.getItem('privateKey');
            if(sk && sk!=''){
              this.privateKeyArr = sk.split(',')
              this.keysize = this.privateKeyArr.length
            }else{
              this.requiredCreateKey = true
              return
            }
            var pk = localStorage.getItem('publicKey');
            if(pk && pk!=''){
              this.publicKeyArr = pk.split(',')
              //this.keysize = this.publicKeyArr.length
            }else{
              this.requiredCreateKey = true
              return
            }
            var mod = localStorage.getItem('modulus');
            if(mod && mod!=''){
              this.modulus = parseInt(mod);
              if(isNaN(this.modulus)){
                this.modulus = 0
                this.requiredCreateKey = true
                return
              }
            }else{
              this.requiredCreateKey = true
              return
            }
            var multi = localStorage.getItem('multiplier');
            if(multi && multi!=''){
              this.multiplier = parseInt(multi);
              if(isNaN(this.multiplier)){
                this.multiplier = 0
                this.requiredCreateKey = true
                return
              }
            }else{
              this.requiredCreateKey = true
              return
            }
            this.keyData.privateKeyArr = this.privateKeyArr;
            this.keyData.publicKeyArr = this.publicKeyArr;
            this.keyData.modulus = this.modulus;
            this.keyData.multiplier = this.multiplier;
            this.keysize = this.privateKeyArr.length;
            this.requiredCreateKey = false;
    
          }else{
            //iOS or Android
            console.log('Mobile');
            storage.get('privateKey').then((private_key) => {
              console.log('Private Key from storage:::' + private_key);
              if(private_key && private_key!=''){
                private_key = String(private_key);
                this.privateKeyArr = private_key.split(',')
                this.keysize = this.privateKeyArr.length
              }else{
                this.requiredCreateKey = true
                return
              }
            })
            storage.get('publicKey').then((public_key) => {
              console.log('Public Key from storage:::' + public_key);
              if(public_key && public_key!=''){
                this.publicKeyArr = public_key.split(',')
                //this.keysize = this.publicKeyArr.length
              }else{
                this.requiredCreateKey = true
                return
              }
            })
            storage.get('modulus').then((modulus) => {
              console.log('Modulus from storage:::' + modulus);
              if(modulus && modulus!=''){
                this.modulus = parseInt(modulus);
                if(isNaN(this.modulus)){
                  this.modulus = 0
                  this.requiredCreateKey = true
                  return
                }
              }else{
                this.requiredCreateKey = true
                return
              }
            })
            storage.get('multiplier').then((multiplier) => {
              console.log('Multiplier from storage:::' + multiplier);
              if(multiplier && multiplier!=''){
                this.multiplier = parseInt(multiplier);
                if(isNaN(this.multiplier)){
                  this.multiplier = 0
                  this.requiredCreateKey = true
                  return
                }
              } else{
                this.requiredCreateKey = true
                return
              }
            })
            this.keyData.privateKeyArr = this.privateKeyArr;
            this.keyData.publicKeyArr = this.publicKeyArr;
            this.keyData.modulus = this.modulus;
            this.keyData.multiplier = this.multiplier;
            this.keysize = this.privateKeyArr.length;
            this.requiredCreateKey = false
          }
}

  ciphertextChanged(event) {
      this.ciphertext = event.value;
  }

  decrypt() {
    if (this.requiredCreateKey){
        // console.log('Decrypt::: KeyDataProvider: ' + this.keyData.getKeysize());
        this.requiredCreateKey = true;
        this.keysize = 8;
        this.modulus = 256;
        this.multiplier = 7;
        this.publicKeyArr = [7,14,28,56,112,224,192,128];
        this.privateKeyArr = [1,2,4,8,16,32,64,128];
    }
    console.log('Decrypt:::Key size: ' + this.keysize);
    console.log('Decrypt:::Modulus: ' + this.modulus);
    console.log('Decrypt:::Multiplier: ' + this.multiplier);
    console.log('Decrypt:::Private Keys: ' + this.privateKeyArr);
    if(this.ciphertext=='') {
        this.showError('No ciphertext given', 'Please enter ciphertext.', 'OK')
        return;
    }else if(this.ciphertext.endsWith(",") || this.ciphertext.endsWith("|")){
        this.showError('Input format error', 'Could not resolve ciphertext(please remove any punctuation at the end).', 'OK')
    }

    // split into array
    var cipherArr:string[] = this.splitText(this.ciphertext);
    console.log("Length of ciphertext: " + cipherArr.length);
    var explainedLines: string = "";
    // var explainedLines: string = "Your ciphertext:\n\n";
    for (var i=0; i<cipherArr.length; i++){
        if (cipherArr[i]==''){
            cipherArr[i] = "0";
        }
        // explainedLines += ("T[" + i + "] = " + cipherArr[i] + "\n");
    }
    // explainedLines += "\n";
    // this.explainedLinesArr.push(explainedLines);

    // decryption process
    var invMultiplier:number = this.xgcd(this.multiplier, this.modulus)[1];
    console.log("Inverse multiplier: " + invMultiplier);
    explainedLines = "Firstly, find the inverse multiplicative of the multiplier ( " + this.multiplier + " ) and the modulus ( " + this.modulus + " )\n\n";
    explainedLines += ("For inverse multiplicative,\n  GCD(" + this.multiplier + "," + this.modulus + ")\n  = (" + this.multiplier + "a + " + this.modulus + "b)\n  = 1\n\n");
    explainedLines += ("GCD(" + this.multiplier + "," + this.modulus + ")\n  = (" + this.multiplier + ")(" + invMultiplier + ") + (" + this.modulus + ")(" + this.xgcd(this.multiplier, this.modulus)[2] + ")\n  = 1\n\n");
    
    if(invMultiplier<0) {
        explainedLines += ("a = " + invMultiplier + ", which is less than 0\n" + invMultiplier + " mod " + this.modulus + " = ");
        invMultiplier+=this.modulus;
        explainedLines += (invMultiplier + "\n\n");
    }
    explainedLines += ("Thus, inverse multiplier is " + invMultiplier + "\n\n");
    this.explainedLinesArr.push(explainedLines);

    var T:any = [];
    for(var i=0; i<cipherArr.length; i++){
        var val = parseInt(cipherArr[i]);
        console.log('T:::push:' + val);
        T.push(val);
    }
    console.log("T:::" + T);

    explainedLines = "Then, apply the formula Y = (W * T[i]) mod p, where W is the inverse multiplier, T is the ciphertext, and p is the modulus.\n\n";
    explainedLines += ("W = " + invMultiplier + "\n");
    explainedLines += ("p = " + this.modulus + "\n\n");
    var Y:any = [];
    for(var j=0; j<T.length; j++) {
        var r = parseInt(T[j]) * invMultiplier % this.modulus;
        console.log('Y:::push:' + r);
        Y.push(r);
        explainedLines += ("T[" + j + "] = " + T[j] + "\n");
        explainedLines += ("Y[" + j + "] = (" + invMultiplier + " * " + T[j] + ") mod " + this.modulus + " = " + r + "\n\n");
    }
    this.explainedLinesArr.push(explainedLines);
    console.log("Y:::" + Y);

    var reverse = "";
    for(var k=T.length-1; k>=0; k--) {
        var val:number = Y[k];
        for (var m = this.privateKeyArr.length - 1; m >= 0; m--) {
            var sk = parseInt(this.privateKeyArr[m]);
            if (val >= sk) {
                reverse += 1;
                val = val - sk;
            } else {
                reverse += 0;
            }
        }
    }
    console.log("Reversed:::" + reverse);
    this.binText = reverse.split("").reverse().join("");
    console.log("Text in binary: " + this.binText);
    //console.log("Check decrypt() binText.length: " + binText.length);
    //console.log("Check decrypt() binText.length % 8: " + (binText.length % 8));
    
    // convert to char
    while ((this.binText.length % 8) != 0){
        this.binText = "0" + this.binText;
    }
    this.decryptedText = this.binarytoString(this.binText);
    console.log("Decrypted text: " + this.decryptedText);

    // Explanation to recover binary of plaintext
    console.log("T.length : " + T.length);
    for (var i=0; i<T.length; i++){
        var binTemp: string = "";
        if (i==0){
            explainedLines = "To recover the binary of the plaintext, take Y[i] to deduct private keys, starting with the largest private key then decreasing down to the smallest private key\n";
            explainedLines += ("Private keys that is used to substract Y[i] procudes binary 1 in plaintext while binary 0 shows that private keys are not being used to minus Y[i]\n\n")
            explainedLines += ("Notes:\n The result of the substraction must be a positive number or 0\n\n");
        }
        else {
            explainedLines = "";
        }
        var val:number = Y[i];
        explainedLines += ("Y[" + i + "] = " + val + "\n\n");
        for (var j=this.privateKeyArr.length-1; j>=0; j--){
            explainedLines += ("PrivateKey[" + j + "] = " + this.privateKeyArr[j] + "\n");
            var result: number = val - this.privateKeyArr[j];
            explainedLines += (val + " - " + this.privateKeyArr[j] + " = " + result + "\n");
            explainedLines += ("b[" + j + "] = ");
            if (result < 0){
                explainedLines += ("0");
                binTemp = "0" + binTemp;
            }
            else if (val!=0){
                val = result;
                explainedLines += ("1");
                binTemp = "1" + binTemp;
            }
            else {
                explainedLines += ("0");
                binTemp = "0" + binTemp;
            }
            explainedLines += ("\n\n");
            if (j==0){
                explainedLines += ("Binary produced from Y[" + i + "] = " + binTemp + "\n\n");
                explainedLines += ("Current binary of plaintext: " + this.binText.substr(0, i*this.keysize+this.keysize) + "\n\n");
                this.explainedLinesArr.push(explainedLines);
            }
        }
    }

    // Explanation to convert binary to character
    for (var i=0; i<this.binText.length-1; i+=8){
        if (i==0){
            explainedLines = "To convert binary to character, split the binary into multiple blocks of 8 bits\n\n";
        }
        else {
            explainedLines = "";
        }
        var binTemp = this.binText.substr(i, 8);
        explainedLines += ("Block " + (i/8) +" = " + binTemp + "\n");
        explainedLines += ("Convert " + binTemp + " to character : " + this.binarytoString(binTemp) + "\n\n");
        explainedLines += ("Current plaintext: " + this.binarytoString(this.binText.substr(0, i+8)) + "\n\n");
        this.explainedLinesArr.push(explainedLines);
    }
    this.explainedLinesArr.push('The decrypted message is ' + this.decryptedText);

    this.buildSlide();
  }

  upload() {
    if(this.platform.is('ios')) {
        this.iosFilePicker.pickFile().then(uri => 
          {console.log(uri)
          var correctPath = uri.substr(0, uri.lastIndexOf('/') + 1);
          var currentName = uri.substring(uri.lastIndexOf('/') + 1);
          var nameParts = currentName.split('.');
              if(nameParts.length>1) {
                var postfix = nameParts[nameParts.length-1];
                console.log('extension:::' + postfix);
                if(postfix!='txt'){
                  //Show error
                  this.showError('File Error', 'Only txt file are supported.', 'OK')
                  return;
                }
              }else{
                this.showError('File Error', 'Invalid file.', 'OK')
                return;
              }
          this.file.readAsText("file:///" + correctPath, currentName).then(result=>{                           
            console.log('File content:::' + result)
            this.ciphertext = result
          })}
        ).catch(err => 
            console.log('Error', err));
    }else if(this.platform.is('android')) {
        this.fileChooser.open().then(uri => 
          {console.log(uri)
          this.filePath.resolveNativePath(uri)
            .then(resolvedUri => 
              {
                console.log(resolvedUri)
                var correctPath = resolvedUri.substr(0, resolvedUri.lastIndexOf('/') + 1);
                var currentName = resolvedUri.substring(resolvedUri.lastIndexOf('/') + 1);
                //console.log('filename:::' + currentName);
                var nameParts = currentName.split('.');
                if(nameParts.length>1) {
                  var postfix = nameParts[nameParts.length-1];
                  console.log('extension:::' + postfix);
                  if(postfix!='txt'){
                    //Show error
                    this.showError('File Error', 'Only txt file are supported.', 'OK')
                    return;
                  }
                }else{
                  this.showError('File Error', 'Invalid file.', 'OK')
                  return;
                }
                this.file.readAsText(correctPath, currentName).then(result=>{                           
                console.log('File content:::' + result)
                this.ciphertext = result
          })
              }).catch(err => console.log(err));
          }
        ).catch(e => console.log(e));
      }
  }

  splitText(text:string){
    var output:string[] = text.split(/[.,\/ \W]/);
    // console.log("Split output: " + output);
    return output;
  }

//   binToText(binText:string){
//     var text:string = "";
//     for (var i = 0; i < binText.length; i+=8){
//         var binary:string = "";
//         for (var j = 0; j < 8; j++){
//             binary += binText[j+i];
//         }
//         if ((i == 0) && (parseInt(binary,2) == 0)){
//             continue;
//         }
//         else {
//             text += String.fromCharCode(parseInt(binary,2));
//         }
//     }
//     return text;
//   }

  xgcd(a, b){
    a = +a;
    b = +b;
    
    if (a !== a || b !== b){
        return [NaN, NaN, NaN];
    }
    
    if (a === Infinity || a === -Infinity || b === Infinity || b === -Infinity){
        return [Infinity, Infinity, Infinity];
    }
    
    // Checks if a or b are decimals
    if ((a % 1 !== 0) || (b % 1 !== 0)){
        return false;
    }
    
    var signX = (a < 0) ? -1 : 1,
    signY = (b < 0) ? -1 : 1,
    x = 0,
    y = 1,
    u = 1,
    v = 0,
    q, r, m, n;
    a = Math.abs(a);
    b = Math.abs(b);
    
    while (a !== 0){
        q = Math.floor(b / a);
        r = b % a;
        m = x - u * q;
        n = y - v * q;
        b = a;
        a = r;
        x = u;
        y = v;
        u = m;
        v = n;
    }
    return [b, signX * x, signY * y];
  }

  binarytoString(s: string){
    var result = "";
    var temp = "";
    for(var i=0;i<s.length;i++){
        if(i%8==0 && i!=0){
            var dec = parseInt(temp,2);
            result+=String.fromCharCode(dec);
            temp=""+s.charAt(i);
        }else{
            temp+=s.charAt(i);
        }
        if(i==s.length-1){
            var dec = parseInt(temp,2);
            result+=String.fromCharCode(dec);
        }
    }
    return result;
  }

  buildSlide() {
    this.showSlide = true;
    this.isEditing = false;
  }

  updateSlide() {
    this.slide.autoHeight = true;
    this.slide.resize();
    if (this.slide.getActiveIndex() == this.slide.length()-1){
      this.slide.stopAutoplay();
    }
  }

  getTotalSlide(){
    this.totalSlide = this.slide.length();
    console.log('Number of slides: ' + this.slide.length());
  }

  updateCurrentSlide(event) {
    this.goToSlideNumber = event.value;
    console.log("Entered slide: " + this.goToSlideNumber);
  }

  goToSlide(){
    if ((this.goToSlideNumber >= 1) && (this.goToSlideNumber <= this.totalSlide)){
      this.slide.slideTo(this.goToSlideNumber-1, 0);
      console.log("Go to slide: " + this.goToSlideNumber);
    }
  }

  showError(title, subTitle, buttonTitle) {
    let alert = this.alertCtrl.create({
        title: title,
        subTitle: subTitle,
        buttons: [buttonTitle]
      });
      alert.present();
  }
}
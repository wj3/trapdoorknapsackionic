//Install Ionic
$npm install ionic
//Install cordova
$npm install cordova
//Install package
$npm install

//To run on Android:
$ionic cordova run Android

//To build on iOS
ionic cordova build ios -- --buildFlag="-UseModernBuildSystem=0"
